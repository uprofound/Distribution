from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import ClientViewSet, DistributionViewSet, MessageAPIView

router = DefaultRouter()
router.register('distributions', DistributionViewSet, basename='distributions')
router.register('clients', ClientViewSet, basename='clients')


urlpatterns = [
    path('v1/', include(router.urls)),
    path(
        'v1/messages/',
        MessageAPIView.as_view()
    ),
]
