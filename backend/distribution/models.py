from django.core.validators import RegexValidator
from django.db import models


class Distribution(models.Model):
    start_date = models.DateTimeField(null=True)
    text = models.CharField(max_length=256)
    filters = models.JSONField(default=dict)
    finish_date = models.DateTimeField(null=True)

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'
        ordering = ['-id']

    def __str__(self):
        return '{}: {}'.format(self.pk, self.text[:32])


class Operator(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
    )
    code = models.SlugField(
        max_length=16,
        unique=True,
    )

    class Meta:
        verbose_name = 'Мобильный оператор'
        verbose_name_plural = 'Мобильные операторы'
        ordering = ['code']

    def __str__(self):
        return self.code


class ClientTag(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
    )
    slug = models.SlugField(
        max_length=32,
        unique=True,
    )

    class Meta:
        verbose_name = 'Тег клиента'
        verbose_name_plural = 'Теги клиентов'
        ordering = ['slug']

    def __str__(self):
        return self.slug


class Client(models.Model):
    phone = models.CharField(
        max_length=11,
        null=False,
        blank=False,
        unique=True,
        validators=[
            RegexValidator(
                regex=r'7[0-9]{10}',
                message=('Номер телефона должен быть в формате '
                         '7XXXXXXXXXX (X - цифра от 0 до 9)')

            )
        ]
    )
    operator_code = models.OneToOneField(
        Operator,
        null=True,
        on_delete=models.SET_NULL
    )
    tag = models.OneToOneField(
        ClientTag,
        null=True,
        on_delete=models.SET_NULL
    )
    # TODO time-zone field

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
        ordering = ['phone']

    def __str__(self):
        return self.phone


class Message(models.Model):
    send_date = models.DateTimeField(auto_now_add=True)
    status = models.SmallIntegerField(null=True)
    distribution = models.ForeignKey(
        Distribution,
        related_name='messages',
        on_delete=models.CASCADE
    )
    client = models.ForeignKey(
        Client,
        related_name='messages',
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ['-distribution', 'client']

    def __str__(self):
        return 'Рассылка {} - клиент {}'.format(
            self.distribution.id, self.client
        )
