from rest_framework.serializers import ModelSerializer, ValidationError

from .models import Client, Distribution, Message


class DistributionSerializer(ModelSerializer):
    FILTER_KEY_CHOICES = ('operator_codes', 'tags')

    class Meta:
        model = Distribution
        fields = ('id', 'start_date', 'text', 'filters', 'finish_date')

    def validate_dates(self, start_date, finish_date):
        if not start_date:
            raise ValidationError(
                'Укажите дату старта рассылки!'
            )
        # TODO проверить соответствие формату
        if finish_date and finish_date < start_date:
            raise ValidationError(
                'Дата окончания рассылки меньше даты старта!'
            )
        return start_date, finish_date

    def validate_filters(self, filters):
        if not filters:
            raise ValidationError(
                'В рассылке должен быть хотя бы один фильтр!'
            )
        for key in filters:
            if key not in self.FILTER_KEY_CHOICES:
                raise ValidationError(
                    ('Укажите правильно ключи фильтрации рассылок, возможные '
                     f'значения: {" и ".join(self.FILTER_KEY_CHOICES)}!')
                )
        return filters

    def validate(self, data):
        data['start_date'], data['finish_date'] = self.validate_dates(
            self.initial_data.get('start_date'),
            self.initial_data.get('finish_date')
        )
        data['filters'] = self.validate_filters(
            self.initial_data.get('filters')
        )
        return data


class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = ('id', 'phone')


class MessageSerializer(ModelSerializer):
    class Meta:
        model = Message
        fields = ('send_date', 'distribution', 'client')
