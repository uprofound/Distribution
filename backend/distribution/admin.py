from django.contrib import admin

from .models import Client, ClientTag, Distribution, Message, Operator


admin.site.register(Client)
admin.site.register(ClientTag)
admin.site.register(Distribution)
admin.site.register(Message)
admin.site.register(Operator)
