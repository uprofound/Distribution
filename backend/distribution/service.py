import os
import requests

from datetime import datetime
from django.shortcuts import get_object_or_404
from dotenv import load_dotenv

from .models import Client, Distribution, Message

load_dotenv()

URL = 'https://probe.fbrq.cloud/v1/send/'
TOKEN = os.getenv('PROBE_TOKEN')


def request_send_message(msg_id, phone, text):
    headers = {'Authorization': f'Bearer {TOKEN}'}
    json = {
        'id': msg_id,
        'phone': phone,
        'text': text
    }
    try:
        response = requests.post(
            url=f'{URL}{msg_id}',
            headers=headers,
            json=json
        )
    except requests.RequestException as exc:
        print(exc.__str__())  # TODO log
    else:
        Message.objects.filter(pk=msg_id).update(status=response.status_code)


def process_distribution(data):
    distribution = get_object_or_404(
        Distribution,
        id=data.get('id')
    )
    # TODO форматы дат
    start_date = data.get('start_date')
    if not start_date or start_date > str(datetime.now()):
        return None
    finish_date = data.get('finish_date')
    if finish_date and finish_date < str(datetime.now()):
        return None

    filters = data.get('filters')
    operator_codes = filters.get('operator_codes')
    tags = filters.get('tags')
    if operator_codes and tags:
        queryset = Client.objects.filter(
            operator_code__code__in=operator_codes,
            tag__slug__in=tags
        )
    elif not tags:
        queryset = Client.objects.filter(
            operator_code__code__in=operator_codes
        )
    else:
        queryset = Client.objects.filter(
            tag__slug__in=tags
        )

    for client in queryset:
        msg = Message.objects.create(
            distribution=distribution,
            client=client
        )
        request_send_message(msg.id, int(client.phone), data.get('text'))
