from rest_framework.generics import ListCreateAPIView
from rest_framework.viewsets import ModelViewSet


from .models import Client, Distribution, Message
from .serializers import ClientSerializer, DistributionSerializer, MessageSerializer
from .service import process_distribution


class DistributionViewSet(ModelViewSet):
    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializer

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        process_distribution(response.data)  # TODO delay
        return response


class ClientViewSet(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MessageAPIView(ListCreateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
