# Distribution - сервис управления рассылками  
Сервис предоставляет API администрирования рассылками и получения статистики.

### Используемые технологии:
Python 3.8.6  
Django 3.2.12  
Django REST Framework 3.12.4  
PostgreSQL 

## Установка приложения локально:

Клонируйте репозиторий и перейдите в него в командной строке:

```bash
git clone git@gitlab.com:uprofound/Distribution.git

cd Distribution
```

Перейдите в каталог backend, создайте и активируйте виртуальное окружение:

```bash
cd backend

python3 -m venv .venv

source env/bin/activate
```

Установите зависимости из файла requirements.txt:

```bash
python3 -m pip install --upgrade pip

pip install -r requirements.txt
```

Подготовьте файл переменных окружения .env -  
скопируйте шаблон из файла .env.template:  

```bash
cp .env.template .env
```

заполните его следующими данными:  

```
SECRET_KEY=your_key  # секретный ключ Django (укажите свой)
PROBE_TOKEN=your_token  # укажите токен аутентификации 
                          на внешнем сервисе отправки сообщений

# Опционально:

# DEBUG=True  # default=False
# ALLOWED_HOSTS=localhost  # перечислить через запятую,
                             default='127.0.0.1'

# Если не заполнять следующее - БД запустится на движке sqlite3
# DB_ENGINE=django.db.backends.postgresql_psycopg2  # движок базы данных
# POSTGRES_DB=postgres_db  # имя базы данных (укажите своё)
# POSTGRES_USER=postgres  # логин для подключения к базе данных (укажите свой)
# POSTGRES_PASSWORD=postgres  # пароль для подключения к БД (установите свой)
# DB_HOST=localhost  # хост сервиса
# DB_PORT=5432  # порт для подключения к БД
```

Выполните миграции:

```bash
python3 manage.py migrate
```

Запустите проект:

```bash
python3 manage.py runserver
```
Теперь сервис доступен по адресу <http://127.0.0.1:8000/api/v1/>,  
а документация по API - по адресу <http://127.0.0.1:8000/docs/>.

### Создание суперпользователя

Администрирование сервиса доступно пользователю с правами админа.  
Для создания суперпользователя выполните команду:

```bash
python3 manage.py createsuperuser
```

Теперь возможна авторизация в админ-зоне по адресу <http://127.0.0.1:8000/admin/>.

___________________________________
*Из дополнительных заданий выполнены:*  
*5. ... страница со Swagger UI с описанием разработанного API.*  
*6. частично: реализовать администраторский Web UI ...*  

